package com.example.examenunidad1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import org.w3c.dom.Text

class CuentaBancoActivity : AppCompatActivity() {

    private lateinit var strBanco: TextView
    private lateinit var lblNombre: TextView
    private lateinit var lblSaldo:  TextView
    private lateinit var lblMovimientos: TextView
    private lateinit var txtCantidad: EditText
    private lateinit var btnDeposito: Button
    private lateinit var btnRetiro: Button
    private lateinit var btnRegresar: Button
    private var movimientos: MutableList<String> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cuenta_banco)
        iniciarComponentes()

        // Obtener los datos del MainActivity
        val datos = intent.extras
        val nombre = datos?.getString("nombre")
        val numCuenta = datos?.getString("numCuenta")
        val banco = datos?.getString("banco")
        val saldo = datos?.getString("saldo")

        // Actualizar los componentes de la interfaz de usuario con los datos recibidos
        strBanco.text = banco
        lblNombre.text = nombre
        lblSaldo.text = saldo

        btnDeposito.setOnClickListener {
            if (validarCampos()) {
                deposito()
            } else {
                mostrarToast("Por favor, completa todos los campos")
            }
        }

        btnRetiro.setOnClickListener {
            if (validarCampos()) {
                retiro()
            } else {
                mostrarToast("Por favor, completa todos los campos")
            }
        }

        btnRegresar.setOnClickListener {
            regresar()
        }
    }

    private fun iniciarComponentes() {
        strBanco = findViewById(R.id.strBanco)
        lblNombre = findViewById(R.id.lblNombre)
        lblSaldo = findViewById(R.id.lblSaldo)
        lblMovimientos = findViewById(R.id.lblMovimientos)
        txtCantidad = findViewById(R.id.txtCantidad)
        btnDeposito = findViewById(R.id.btnDeposito)
        btnRetiro = findViewById(R.id.btnRetiro)
        btnRegresar = findViewById(R.id.btnRegresar)
    }

    private fun validarCampos(): Boolean {
        val cantidad = txtCantidad.text.toString()
        return cantidad.isNotEmpty()
    }

    private fun deposito() {
        val cantidad = txtCantidad.text.toString().toFloat()
        val saldoActual = lblSaldo.text.toString().toFloat()
        val nuevoSaldo = saldoActual + cantidad

        lblSaldo.text = nuevoSaldo.toString()
        val movimiento = "Depósito de $cantidad"
        movimientos.add(movimiento)
        actualizarMovimientos()
        mostrarToast("Depósito realizado correctamente")
    }

    private fun retiro() {
        val cantidad = txtCantidad.text.toString().toFloat()
        val saldoActual = lblSaldo.text.toString().toFloat()

        if (cantidad > saldoActual) {
            mostrarToast("No hay suficiente saldo para realizar el retiro")
        } else {
            val nuevoSaldo = saldoActual - cantidad
            lblSaldo.text = nuevoSaldo.toString()
            val movimiento = "Retiro de $cantidad"
            movimientos.add(movimiento)
            actualizarMovimientos()
            mostrarToast("Retiro realizado correctamente")
        }
    }

    private fun actualizarMovimientos() {
        lblMovimientos.text = movimientos.joinToString("\n\n")
    }
    private fun regresar() {
        val confirmar = AlertDialog.Builder(this)
        confirmar.setTitle("Banco")
        confirmar.setMessage("Regresar al MainActivity?")
        confirmar.setPositiveButton("Confirmar") { dialog, _ ->
            finish()
        }
        confirmar.setNegativeButton("Cancelar") { dialog, _ ->
            dialog.dismiss()
        }
        confirmar.show()
    }

    private fun mostrarToast(mensaje: String) {
        Toast.makeText(applicationContext, mensaje, Toast.LENGTH_SHORT).show()
    }
}