package com.example.examenunidad1

class CuentaBanco (
    var numCuenta: Int,
    var nombre: String,
    var banco: String,
    var saldo: Float
) {
    private val movimientos: MutableList<String> = mutableListOf()

    fun depositar(cantidad: Float) {
        saldo += cantidad
        val movimiento = "Depósito de $cantidad"
        registrarMovimiento(movimiento)
    }

    fun retirar(cantidad: Float): Boolean {
        if (saldo >= cantidad) {
            saldo -= cantidad
            val movimiento = "Retiro de $cantidad"
            registrarMovimiento(movimiento)
            return true
        } else {
            return false
        }
    }

    private fun registrarMovimiento(movimiento: String) {
        movimientos.add(movimiento)
    }

    fun obtenerMovimientos(): List<String> {
        return movimientos
    }
}