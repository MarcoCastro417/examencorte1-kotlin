package com.example.examenunidad1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.google.android.material.textfield.TextInputEditText

class MainActivity : AppCompatActivity() {

    private lateinit var btnEnviar: Button
    private lateinit var btnSalir: Button
    private lateinit var txtNumCuenta: TextInputEditText
    private lateinit var txtNombre: TextInputEditText
    private lateinit var txtBanco: TextInputEditText
    private lateinit var txtSaldo: TextInputEditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        iniciarComponentes()

        btnEnviar.setOnClickListener {
            btnEnviar()
        }

        btnSalir.setOnClickListener {
            btnSalir()
        }
    }

    private fun iniciarComponentes() {
        btnEnviar = findViewById(R.id.btnEnviar)
        btnSalir = findViewById(R.id.btnSalir)
        txtNumCuenta = findViewById(R.id.txtNumCuenta)
        txtNombre = findViewById(R.id.txtNombre)
        txtBanco = findViewById(R.id.txtBanco)
        txtSaldo = findViewById(R.id.txtSaldo)
    }

    private fun btnEnviar() {
        val strBanco = getString(R.string.strBanco)
        val txtNumCuentaValue = txtNumCuenta.text.toString()
        val txtNombreValue = txtNombre.text.toString()
        val txtSaldoValue = txtSaldo.text.toString()

        if (txtNumCuentaValue.isEmpty() || txtNombreValue.isEmpty() || txtSaldoValue.isEmpty() || !(strBanco == txtBanco.text.toString())) {
            // Code block executed when any field is empty or the bank is not valid
            Toast.makeText(applicationContext, "Falta información en algunos campos o el banco no es válido", Toast.LENGTH_SHORT).show()
        } else {
            // Code block executed when all fields are filled and the bank is valid
            val bundle = Bundle()
            bundle.putString("numCuenta", txtNumCuentaValue)
            bundle.putString("nombre", txtNombreValue)
            bundle.putString("banco", strBanco)
            bundle.putString("saldo", txtSaldoValue)

            val intent = Intent(this, CuentaBancoActivity::class.java)
            intent.putExtras(bundle)

            startActivity(intent)
        }
    }

    private fun btnSalir() {
        val confirmar = AlertDialog.Builder(this)
        confirmar.setTitle("Banco")
        confirmar.setMessage("Cerrar la aplicación?")
        confirmar.setPositiveButton("Confirmar") { dialog, _ ->
            finish()
        }
        confirmar.setNegativeButton("Cancelar") { dialog, _ ->
            dialog.dismiss()
        }
        confirmar.show()
    }
}